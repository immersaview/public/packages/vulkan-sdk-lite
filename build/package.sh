#!/usr/bin/env bash

# Usage:
# package <imv source directory>

# Exit as failure if any command fails
set -e

# Enable ** to glob recursively
shopt -s globstar

BASE_DIR="`cd \"\`dirname \"${BASH_SOURCE}\"\`\" && pwd`"
if [[ ! -z "${1}" ]]; then
    IMV_SOURCE_DIR="`cd "${1}" && pwd`"
fi

echo "=============================================="
echo "Local Package"
echo "=============================================="

pushd "${BASE_DIR}/../NuGet" > /dev/null
    nuget pack

    if [[ -d "${IMV_SOURCE_DIR}" ]]; then
        rm -fr "${IMV_SOURCE_DIR}/packages/Imv.External.vulkan-sdk-lite"*
        echo "Installing Imv.External.vulkan-sdk-lite to ${IMV_SOURCE_DIR}/packages/"
        nuget install Imv.External.vulkan-sdk-lite -Source "`pwd`" -OutputDirectory "${IMV_SOURCE_DIR}/packages/" -NonInteractive -Verbosity quiet -NoCache
    fi

    bash ./check_package_exists.sh || ( echo "Package version already exists" && exit 1 )
popd > /dev/null