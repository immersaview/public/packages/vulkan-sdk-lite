# Vulkan SDK Lite

A stripped down version of the LunarG Vulkan SDK containing only the things we need to build vulkan applications (headers, lib and shader compiler) in the pipeline.

## In this repo

- Vulkan headers
- Vulkan libs
- glm headers (Vector math library)
- GLSL shader compiler
- Vulkan logo

## Updating
This repo contains basically a straight copy of the packaged LunarG Vulkan SDK minus a lot of stuff we don't need for building in the pipeline. To update to a new version download the new [LunarG SDK](https://vulkan.lunarg.com/) and install, then copy the following files/directories into the "SDK" folder:

- Bin/glslangValidator.exe
- Bin/glslc.exe
- Bin32/glslangValidator.exe
- Bin32/glslc.exe
- Include/vk_video/*
- Include/vulkan/*
- Lib/VkLayer_utils.lib
- Lib/vulkan-1.lib
- Lib32/VkLayer_utils.lib
- Lib32/vulkan-1.lib
- Third-Party/Include/glm/*
- Third-Party/README.md
- LICENSE.txt
- README.txt
- vulkan.bmp
- vulkan.ico