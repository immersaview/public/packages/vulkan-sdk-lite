cmake_minimum_required(VERSION 3.0)

set(VULKAN_SDK_DIR "${CMAKE_CURRENT_LIST_DIR}" CACHE PATH "Directory in which the Vulkan SDK can be found.")

mark_as_advanced(VULKAN_SDK_DIR)

# Equivalent to FindVulkan() but using the NuGet package contents instead of installed Vulkan SDK
if(CMAKE_SIZEOF_VOID_P EQUAL 8)
    find_library(Vulkan_LIBRARY
      NAMES vulkan-1
      HINTS
        "${VULKAN_SDK_DIR}/Lib"
      )
elseif(CMAKE_SIZEOF_VOID_P EQUAL 4)
    find_library(Vulkan_LIBRARY
      NAMES vulkan-1
      HINTS
        "{VULKAN_SDK_DIR}/Lib32"
      )
endif()


if(WIN32)
  find_path(Vulkan_INCLUDE_DIR
    NAMES vulkan/vulkan.h
    HINTS
      "${VULKAN_SDK_DIR}/Include"
    )

  if(CMAKE_SIZEOF_VOID_P EQUAL 8)
    find_library(Vulkan_LIBRARY
      NAMES vulkan-1
      HINTS
        "${VULKAN_SDK_DIR}/Lib"
        "${VULKAN_SDK_DIR}/Bin"
      )
    find_program(Vulkan_GLSLC_EXECUTABLE
      NAMES glslc
      HINTS
        "${VULKAN_SDK_DIR}/Bin"
      )
    find_program(Vulkan_GLSLANG_VALIDATOR_EXECUTABLE
      NAMES glslangValidator
      HINTS
        "${VULKAN_SDK_DIR}/Bin"
      )
  elseif(CMAKE_SIZEOF_VOID_P EQUAL 4)
    find_library(Vulkan_LIBRARY
      NAMES vulkan-1
      HINTS
        "${VULKAN_SDK_DIR}/Lib32"
        "${VULKAN_SDK_DIR}/Bin32"
      )
    find_program(Vulkan_GLSLC_EXECUTABLE
      NAMES glslc
      HINTS
        "${VULKAN_SDK_DIR}/Bin32"
      )
    find_program(Vulkan_GLSLANG_VALIDATOR_EXECUTABLE
      NAMES glslangValidator
      HINTS
        "${VULKAN_SDK_DIR}/Bin32"
      )
  endif()
else()
  find_path(Vulkan_INCLUDE_DIR
    NAMES vulkan/vulkan.h
    HINTS "${VULKAN_SDK_DIR}/include")
  find_library(Vulkan_LIBRARY
    NAMES vulkan
    HINTS "${VULKAN_SDK_DIR}/lib")
  find_program(Vulkan_GLSLC_EXECUTABLE
    NAMES glslc
    HINTS "${VULKAN_SDK_DIR}/bin")
  find_program(Vulkan_GLSLANG_VALIDATOR_EXECUTABLE
    NAMES glslangValidator
    HINTS "${VULKAN_SDK_DIR}/bin")
endif()

set(Vulkan_LIBRARIES ${Vulkan_LIBRARY})
set(Vulkan_INCLUDE_DIRS ${Vulkan_INCLUDE_DIR})

# assume that we actually found all the Vulkan stuff we provide in the NuGet package for now!
set(Vulkan_FOUND ON CACHE BOOL "A boolean indicating whether Vulkan dependencies have been found.")

mark_as_advanced(Vulkan_INCLUDE_DIR Vulkan_LIBRARY Vulkan_GLSLC_EXECUTABLE
                 Vulkan_GLSLANG_VALIDATOR_EXECUTABLE Vulkan_FOUND)

set(GLSLANG_COMMAND "${VULKAN_SDK_DIR}/Bin/glslangvalidator" CACHE INTERNAL "GLSL compiler path")